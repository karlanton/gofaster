﻿using UnityEngine;
using System.Collections;

public class EnemyView : MonoBehaviour
{
    public EnemyMove Enemy;
    public GameSettings Settings { get { return Game.Instance.Settings; } }
    bool _Dying;
    bool _Spawning;
    float _SpawnDeadline;
    public float SavedSpeed;

    void OnCollisionEnter2D(Collision2D collision)
    {
        var rb = GetComponent<Rigidbody2D>();
        rb.velocity = rb.velocity.normalized * SavedSpeed;
    }


    public bool GetVisible()
    {
        var viewportPos = Camera.main.WorldToViewportPoint(transform.position);
        return viewportPos.x >= 0f && viewportPos.x <= 1f
            && viewportPos.y >= 0f && viewportPos.y <= 1f;
    }

    void FixedUpdate()
    {
        float topSpeed = Game.Instance.GetTopSpeed();
        Animator animator = GetComponent<Animator>();
        animator.SetBool("Killable", topSpeed > Enemy.GetRequiredSpeed());
        var currentState = animator.GetCurrentAnimatorStateInfo(0);
        if (_Dying)
        {
            bool playingAnim = false;
            if (animator != null)
            {
                if (currentState.IsName("Exit") == false)
                    playingAnim = true;
            }
            if (!playingAnim)
                GameObject.Destroy(this.gameObject);
        }
        else if (_Spawning)
        {
            Debug.Log("_Deadline " + _SpawnDeadline + " " + Time.timeSinceLevelLoad);
            if (_SpawnDeadline < Time.timeSinceLevelLoad)
            {
                _Spawning = false;
            }
        }
        animator.SetBool("Spawning", _Spawning);
        var collider = GetComponent<Collider2D>();
        collider.enabled = _Spawning == false && _Dying == false;
        var rb = GetComponent<Rigidbody2D>();
        SavedSpeed = rb.velocity.magnitude;
        rb.angularVelocity = 0f;
    }

    public void Spawn()
    {
        var collider = GetComponent<Collider2D>();
        collider.enabled = false;
        _Spawning = true;
        Animator animator = GetComponent<Animator>();
        if (animator != null)
        {
            animator.SetBool("Spawning", true);
        }
        _SpawnDeadline = Time.timeSinceLevelLoad + Settings.SpawnTime;
    }

    public void Die()
    {
        var collider = GetComponent<Collider2D>();
        collider.enabled = false;
        _Dying = true;
        Animator animator = GetComponent<Animator>();
        if (animator != null)
        {
            animator.SetBool("Dead", true);
        }
        Game.Instance.PlayAudio(GameSettings.GetRandomClip(Settings.Sound.EnemyDeath), Settings.Sound.EnemyDeathVolume);
    }
}

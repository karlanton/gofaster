﻿using UnityEngine;
using System.Collections;

public class PlayerMove : MonoBehaviour
{
    public GameSettings Settings;

    public string XAxis;
    public string YAxis;
    public GameObject ViewPrefab;
    public Vector2 StartPos;
    public float BonusSpeed;
    float _Direction;
    bool _HandledCollision;

    public PlayerView View;
    public float SavedSpeed;

    float _RecoveryTimeLeft;
    bool _Recovering;




    void Start()
    {
        var viewGO = GameObject.Instantiate(ViewPrefab, StartPos, Quaternion.LookRotation(new Vector3(0, 0f, 1f))) as GameObject;
        View = viewGO.GetComponent<PlayerView>();
        View.OnCollision += OnColliderEnter2D;
        View.Player = this;
        viewGO.name = "View of " + gameObject.name;
        var rb = View.GetComponent<Rigidbody2D>();
        rb.velocity = new Vector2(Settings.StartSpeed + BonusSpeed, 0);
    }


    public static Vector2 DoWrapping(Vector2 targetPos)
    {
        float orthoSize = Camera.main.orthographicSize;
        float width = orthoSize * Camera.main.aspect;
        Rect mapRect = new Rect(-width, -orthoSize, width * 2, orthoSize * 2);

        if (targetPos.x > mapRect.xMax
            || targetPos.x < mapRect.xMin
            || targetPos.y > mapRect.yMax
            || targetPos.y < mapRect.yMin)
        {
            // todo check collisions when moving offscreen
            float xNeg = Mathf.Min(0f, mapRect.xMin);
            float yNeg = Mathf.Min(0f, mapRect.yMin);
            targetPos.x = Mathf.Repeat(targetPos.x - xNeg, mapRect.width) + xNeg;
            targetPos.y = Mathf.Repeat(targetPos.y - yNeg, mapRect.height) + yNeg;
        }
        return targetPos;
    }

    void FixedUpdate()
    {

        var viewTransform = View.transform;
        var rb = viewTransform.GetComponent<Rigidbody2D>();
        float inputX = Input.GetAxis(XAxis);
        float inputY = -Input.GetAxis(YAxis);
        Vector2 inputDir = new Vector2(inputX, inputY);
        if (inputDir != Vector2.zero)
        {
            inputDir.Normalize();
            _Direction = Mathf.Rad2Deg * Mathf.Atan2(inputDir.y, inputDir.x);
        }
        if (!_Recovering)
        {
            float currentAngle = rb.rotation;
            float newAngle = Mathf.MoveTowardsAngle(currentAngle, _Direction, Settings.TurnSpeed * Time.deltaTime * Mathf.Max(Mathf.Abs(inputY), Mathf.Abs(inputX)));
            rb.MoveRotation(newAngle);

            float speed = rb.velocity.magnitude;
            float acceleration = Settings.Acceleration.Evaluate(speed);
            float viewRot = viewTransform.rotation.eulerAngles.z;
            Vector2 velocityNormalized = new Vector2(Mathf.Cos(Mathf.Deg2Rad * viewRot), Mathf.Sin(Mathf.Deg2Rad * viewRot));

            Vector2 targetVelocity = velocityNormalized * speed;

            rb.velocity = targetVelocity + acceleration * Time.deltaTime * velocityNormalized;
        }
        else
        {
            _RecoveryTimeLeft -= Time.deltaTime;
            if (_RecoveryTimeLeft <= 0f)
                _Recovering = false;

            if (float.IsInfinity(rb.angularVelocity))
                rb.angularVelocity = 0f;
            rb.angularVelocity = Mathf.Lerp(rb.angularVelocity, 0f, 0.25f);
            rb.velocity *= 1 - (Settings.RecoveryDeacceleration.Evaluate(_RecoveryTimeLeft / Settings.RecoveryTime) * Time.deltaTime);
        }
        Vector3 targetPos = viewTransform.position;
        Vector3 wrappedPos = DoWrapping(targetPos);
        if (wrappedPos != targetPos)
        {
            var saveRot = viewTransform.rotation;
            var saveVel = rb.velocity;
            var angularVelocity = rb.angularVelocity;
            Destroy(View.gameObject);

            var newViewGO = GameObject.Instantiate(ViewPrefab, wrappedPos, saveRot) as GameObject;
            View = newViewGO.GetComponent<PlayerView>();
            View.OnCollision += OnColliderEnter2D;
            View.Player = this;
            newViewGO.name = "View of " + gameObject.name;
            rb = View.GetComponent<Rigidbody2D>();
            rb.velocity = saveVel;
            rb.angularVelocity = angularVelocity;
        }
        _HandledCollision = false;
        SavedSpeed = rb.velocity.magnitude;
        if (View.Trail != null)
        {
            View.Trail.emissionRate = Settings.EmissionOverSpeed.Evaluate(SavedSpeed);
        }
    }

    void OnColliderEnter2D(Collision2D collision)
    {
        var rb = View.GetComponent<Rigidbody2D>();

        var delta = rb.transform.position - collision.transform.position;
        if (delta != Vector3.zero)
            delta.Normalize();
        Vector2 forward = new Vector2(Mathf.Cos(Mathf.Deg2Rad * rb.rotation), Mathf.Sin(Mathf.Deg2Rad * rb.rotation));
        Vector2 otherForward = new Vector2(Mathf.Cos(Mathf.Deg2Rad * collision.rigidbody.rotation), Mathf.Sin(Mathf.Deg2Rad * collision.rigidbody.rotation));
        bool behind = Vector2.Dot(delta, forward) < 0;

        var otherPlayerView = collision.gameObject.GetComponent<PlayerView>();
        var otherPlayer = otherPlayerView != null ? otherPlayerView.Player : null;
        if (otherPlayer != null
            && otherPlayer._HandledCollision == false)
        {
            var primaryPoint = collision.contacts[0];
            _HandledCollision = true;

            bool otherBehind = Vector2.Dot(delta, otherForward) > 0;
            Rigidbody2D aheadRB = behind ? collision.rigidbody : rb;
            Rigidbody2D behindRB = behind ? rb : collision.rigidbody;

            if (behind && otherBehind)
            {
                DoSlowResolution(aheadRB, behindRB);
                DoSlowResolution(behindRB, aheadRB);
                Game.Instance.PlayAudio(GameSettings.GetRandomClip(Settings.BadImpactClips), Settings.BadImpactVolume);
                Game.Instance.PlayAudio(GameSettings.GetRandomClip(Settings.BadImpactWoosh), Settings.BadImpactWooshVolume);

                var animator = behindRB.GetComponent<Animator>();
                animator.Play("BadImpact");
                animator = aheadRB.GetComponent<Animator>();
                animator.Play("BadImpact");
            }
            else
            {
                DoSlowResolution(behindRB, aheadRB);
                DoAheadResolution(aheadRB, aheadRB.GetComponent<PlayerView>().Player, behindRB, behindRB.GetComponent<PlayerView>().Player);
                var animator = aheadRB.GetComponent<Animator>();
                animator.Play("Boost");
                Game.Instance.PlayAudio(GameSettings.GetRandomClip(Settings.GoodImpactClips), Settings.GoodImpactVolume);
                Game.Instance.PlayAudio(GameSettings.GetRandomClip(Settings.SpeedClips), Settings.SpeedClipVolume);
            }
        }
        var enemy = collision.gameObject.GetComponent<EnemyView>();
        if (enemy != null)
        {
            if (enemy.Enemy.DummyEnemy)
            {
                DoSlowResolution(rb, collision.rigidbody);
                collision.rigidbody.velocity = collision.rigidbody.velocity.normalized * enemy.SavedSpeed;
                Game.Instance.PlayAudio(GameSettings.GetRandomClip(Settings.BadImpactClips), Settings.BadImpactVolume);
                Game.Instance.PlayAudio(GameSettings.GetRandomClip(Settings.BadImpactWoosh), Settings.BadImpactWooshVolume);

                var animator = rb.GetComponent<Animator>();
                animator.Play("BadImpact");
            }
            else
            {
                float currentSpeed = Game.Instance.GetTopSpeed();
                float requiredSpeed = enemy.Enemy.GetRequiredSpeed();
                if (requiredSpeed < currentSpeed)
                {
                    rb.velocity = rb.velocity.normalized * SavedSpeed;
                    collision.rigidbody.velocity = rb.velocity * 0.75f;
                    GameObject.Destroy(enemy.Enemy.gameObject);
                }
                else
                {
                    Game.Instance.GameOver();
                }
            }
        }
    }

    private void DoAheadResolution(Rigidbody2D aheadRB, PlayerMove player, Rigidbody2D behindRB, PlayerMove behindPlayer)
    {
        Vector2 aheadDirection = aheadRB.velocity.normalized;
        float maxSpeed = Mathf.Max(player.SavedSpeed, behindPlayer.SavedSpeed);
        float increase = Settings.BoostSpeedIncrease.Evaluate(maxSpeed);
        aheadRB.velocity = Mathf.Max(0, maxSpeed + increase) * aheadDirection;
        var emitPart = aheadRB.GetComponentInChildren<ParticleSystem>();
        emitPart.Emit(10);
        player._Recovering = false;
    }

    private void DoSlowResolution(Rigidbody2D behindRB, Rigidbody2D aheadRB)
    {
        PlayerMove behindPlayer = behindRB.GetComponent<PlayerView>().Player;
        float speed = behindPlayer.SavedSpeed;
        Vector2 behindDirection = (behindRB.transform.position - aheadRB.transform.position).normalized;
        behindRB.velocity = speed * behindDirection;

        behindPlayer._Recovering = true;
        behindPlayer._RecoveryTimeLeft = Settings.RecoveryTime;
    }
}

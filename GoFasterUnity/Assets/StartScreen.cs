﻿using UnityEngine;
using System.Collections;

public class StartScreen : MonoBehaviour
{
    public AudioSource MusicLoop;
    public static AudioSource ActiveMusicLoop;
    public GameObject GameRoot;
    bool _Transitioning;
    void Awake()
    {
        GameRoot.SetActive(false);
    }
    void Start()
    {
        if (ActiveMusicLoop == null)
        {
            DontDestroyOnLoad(MusicLoop);
            MusicLoop.Play();
            ActiveMusicLoop = MusicLoop;
        }
        else
        {
            Object.Destroy(MusicLoop);
        }

    }

    IEnumerator DoTransition()
    {
        var animator = GetComponent<Animator>();
        animator.SetBool("Start", true);
        GameRoot.SetActive(true);

        while (true)
        {
            var state = animator.GetCurrentAnimatorStateInfo(0);
            if (state.IsName("Exit"))
                break;
            yield return null;
        }
        Destroy(this.gameObject);
    }

    void Update()
    {
        if (Input.anyKey && _Transitioning == false)
        {
            _Transitioning = true;
            StartCoroutine(DoTransition());
        }
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Game : MonoBehaviour
{
    public static Game Instance;
    public static int Score;

    public GameSettings Settings;

    struct ActiveAudioState
    {
        public int ID;
        public AudioSource Source;
    }
    int _AudioIDSource;
    List<ActiveAudioState> _ActiveSounds;
    Stack<AudioSource> _SourcePool;

    PlayerMove[] _Players;
    int _CurrentLevel;

    float _SpawnAccumulator;
    float _ZoomOutDeadline;
    float _ZoomTarget;
    float _ZoomStart;
    void Awake()
    {
        _SourcePool = new Stack<AudioSource>();
        _ActiveSounds = new List<ActiveAudioState>();
        _Players = Object.FindObjectsOfType<PlayerMove>();
        Instance = this;
        Camera.main.orthographicSize = Settings.Config[0].MapSize;
    }

    float _Duration;

    void Update()
    {
        float maxSpeed = 0f;
        for (int i = 0; i < _Players.Length; i++)
        {
            var rb = _Players[i].View.GetComponent<Rigidbody2D>();
            maxSpeed = Mathf.Max(rb.velocity.magnitude, maxSpeed);
        }
        int currentLevel = 0;
        for (int i = 0; i < Settings.Config.Length; i++)
        {
            var conf = Settings.Config[i];
            if (maxSpeed > conf.Speed)
                currentLevel = i;
        }
        if (currentLevel > _CurrentLevel)
        {
            _CurrentLevel = currentLevel;
            var conf = Settings.Config[currentLevel];
            _ZoomOutDeadline = Time.timeSinceLevelLoad + Settings.ZoomTime;
            _ZoomTarget = conf.MapSize;
            _ZoomStart = Camera.main.orthographicSize;
        }
        if (_ZoomOutDeadline > 0)
        {
            float t = Mathf.Clamp01((Time.timeSinceLevelLoad - _ZoomOutDeadline) / Settings.ZoomTime);
            Camera.main.orthographicSize = Mathf.Lerp(_ZoomStart, _ZoomTarget, t);
        }
        _Duration += Time.deltaTime;
        if (_Duration >= Settings.SpawnStartTime)
        {
            DoEnemySpawning();
        }

        CheckSounds();

        Score = Mathf.RoundToInt(Mathf.Max(Score, GetTopSpeed() * Settings.SpeedScoreMultiplier));
    }

    private void CheckSounds()
    {
        for (int i = _ActiveSounds.Count - 1; i >= 0; i--)
        {
            var state = _ActiveSounds[i];
            if (state.Source.isPlaying == false)
            {
                state.Source.gameObject.SetActive(false);
                _SourcePool.Push(state.Source);
                _ActiveSounds.RemoveAt(i);
            }
        }
    }


    public void StopAudio(int id)
    {
        for (int i = 0; i < _ActiveSounds.Count; i++)
        {
            var state = _ActiveSounds[i];
            if (state.ID == id)
                state.Source.Stop();
        }
    }

    public int PlayAudio(AudioClip clip, float volume)
    {
        AudioSource source;
        if (_SourcePool.Count > 0)
        {
            source = _SourcePool.Pop();
        }
        else
        {
            source = GameObject.Instantiate(Settings.AudioSourcePrefab) as AudioSource;
        }
        if (source.gameObject.activeSelf == false)
            source.gameObject.SetActive(true);

        source.volume = volume;
        source.clip = clip;
        source.Play();
        ActiveAudioState state;
        state.Source = source;
        state.ID = _AudioIDSource++;
        _ActiveSounds.Add(state);
        return state.ID;
    }

    private void DoEnemySpawning()
    {
        var conf = Settings.Config[_CurrentLevel];

        _SpawnAccumulator += conf.Speed * Time.deltaTime;
        bool closeToEdge = false;
        for (int i = 0; i < _Players.Length; i++)
        {
            var pos = _Players[i].View.transform.position;
            var viewportPos = Camera.main.WorldToViewportPoint(pos);
            if (viewportPos.x < 0.1f || viewportPos.x > 0.9f
                || viewportPos.y < 0.1f || viewportPos.y > 0.9f)
            {
                closeToEdge = true;
            }
        }
        if (_SpawnAccumulator > 0f && closeToEdge == false)
        {
            float totalSpawnChance = 0f;
            for (int i = 0; i < Settings.Spawns.Length; i++)
            {
                if (Settings.Spawns[i].MinLevel <= _CurrentLevel)
                totalSpawnChance += Settings.Spawns[i].SpawnChance;
            }

            float randomVal = Random.Range(0f, 1f);
            float spawnChanceAccum = 0f;
            for (int i = 0; i < Settings.Spawns.Length; i++)
            {
                float relativeSpawnChance = Settings.Spawns[i].SpawnChance / totalSpawnChance;
                if (Settings.Spawns[i].MinLevel <= _CurrentLevel && randomVal < relativeSpawnChance + spawnChanceAccum)
                {
                    float t = Random.Range(0f, 1f);
                    float speed = Mathf.Lerp(Settings.Spawns[i].SpeedMin, Settings.Spawns[i].SpeedMax, t);
                    float size = Mathf.Lerp(Settings.Spawns[i].MinSize, Settings.Spawns[i].MaxSize, t);
                    float killSpeed = Mathf.Lerp(Settings.Spawns[i].KillSpeedMin, Settings.Spawns[i].KillSpeedMax, t);
                    SpawnEnemy(Settings.Spawns[i].Enemy, speed, size, killSpeed);
                    _SpawnAccumulator -= Settings.Spawns[i].SpawnValue;
                    break;
                }
                spawnChanceAccum += relativeSpawnChance;
            }
        }
    }

    void SpawnEnemy(EnemyView enemy, float speed, float size, float killSpeed)
    {
        Vector2 viewportPos = new Vector2(Random.Range(0f, 1f), Random.Range(0f, 1f));
        if (viewportPos == Vector2.zero)
            viewportPos = Vector2.one;
        if (viewportPos.x > viewportPos.y)
            viewportPos.x = 1f;
        else
            viewportPos.y = 1f;

        Vector2 direction = -new Vector2(viewportPos.x - 0.5f, viewportPos.y - 0.5f).normalized;

        Vector2 worldPos = Camera.main.ViewportToWorldPoint(new Vector2(viewportPos.x, viewportPos.y));
        EnemyMove instance = GameObject.Instantiate(Settings.EnemyPrefab, worldPos, Quaternion.Euler(direction)) as EnemyMove;
        instance.ViewPrefab = enemy;
        instance.Speed = direction * speed;
        instance.Size = size;
        instance.KillSpeed = killSpeed;
    }

    public float GetTopSpeed()
    {
        float speed = 0f;
        for (int i = 0; i < _Players.Length; i++)
        {
            speed = Mathf.Max(_Players[i].SavedSpeed, speed);
        }
        return speed;
    }

    public void GameOver()
    {
        //Debug.Break();
        Application.LoadLevel("ScoreScreen");
    }
}

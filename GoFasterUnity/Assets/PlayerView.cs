﻿using UnityEngine;
using System.Collections;

public class PlayerView : MonoBehaviour
{
    public PlayerMove Player;
    public ParticleSystem Trail;

    public event System.Action<Collision2D> OnCollision;
    void OnCollisionEnter2D(Collision2D collision)
    {
        if (OnCollision != null)
            OnCollision(collision);
    }
}

﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct LevelConfigData
{
    public float Speed;
    public float MapSize;
    public float SpawnSpeed;
}

[System.Serializable]
public struct SpawnPossibility
{
    public int MinLevel;
    public float SpawnValue;
    public float SpawnChance;
    public EnemyView Enemy;
    public float SpeedMin;
    public float SpeedMax;
    public float MinSize;
    public float MaxSize;
    public float KillSpeedMin;
    public float KillSpeedMax;
}


public class GameSettings : ScriptableObject
{
    public AudioClip[] GoodImpactClips;
    public float GoodImpactVolume = 0.7f;

    public AudioClip[] SpeedClips;
    public float SpeedClipVolume = 1f;

    public AudioClip[] BadImpactClips;
    public float BadImpactVolume = 1f;
    public AudioClip[] BadImpactWoosh;
    public float BadImpactWooshVolume = 0.5f;

    [System.Serializable]
    public class SoundSettings
    {
        public AudioClip[] EnemyDeath;
        public float EnemyDeathVolume = 1f;
    }

    public SoundSettings Sound = new SoundSettings();


    public AudioSource AudioSourcePrefab;

    public LevelConfigData[] Config;
    public AnimationCurve Acceleration;
    public AnimationCurve RecoveryDeacceleration;
    public SpawnPossibility[] Spawns;
    public EnemyMove EnemyPrefab;
    public float TurnSpeed;
    public float StartSpeed = 2;
    public float RecoveryTime = 2.5f;
    public float ZoomTime = 0.35f;
    public AnimationCurve BoostSpeedIncrease;
    public AnimationCurve EmissionOverSpeed;
    public float SpawnTime = 1f;
    public float SpeedScoreMultiplier = 100f;
    public float SpawnStartTime = 5f;
#if UNITY_EDITOR
    [UnityEditor.MenuItem("Assets/Create/Game Settings")]
    static void CreateGameSettings()
    {
        GameSettings settings = ScriptableObject.CreateInstance<GameSettings>();
        UnityEditor.AssetDatabase.CreateAsset(settings, "Assets/Game Settings.asset");
    }
#endif

    public static AudioClip GetRandomClip(AudioClip[] clip)
    {
        return clip[Random.Range(0, clip.Length)];
    }
}

﻿using UnityEngine;
using System.Collections;

public class EnemyMove : MonoBehaviour
{
    public EnemyView ViewPrefab;
    public EnemyView View;
    public Vector2 Speed;
    public float Size;
    public float KillSpeed;
    public bool DummyEnemy;


    void Start()
    {
        View = GameObject.Instantiate(ViewPrefab, transform.position, transform.rotation) as EnemyView;
        View.Enemy = this;
        View.transform.localScale = View.transform.localScale * Size;
        var rb = View.GetComponent<Rigidbody2D>();
        rb.velocity = Speed;
        View.gameObject.SetActive(false);
        View.Spawn();
    }

    void FixedUpdate()
    {
        var rb = View.GetComponent<Rigidbody2D>();

        bool visible = View.GetVisible();
        if (visible && View.gameObject.activeSelf == false)
        {
            View.gameObject.SetActive(true);
            rb.velocity = Speed;
        }

        if (View.gameObject.activeSelf)
        {
            Vector2 pos = View.transform.position;
            Vector2 wrappedPos = PlayerMove.DoWrapping(pos);
            if (pos != wrappedPos)
                View.transform.position = wrappedPos;
        }
    }

    public float GetRequiredSpeed()
    {
        return KillSpeed;
    }
    void OnDestroy()
    {
        View.Die();
    }
}
